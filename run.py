import discord, asyncio, os
import textData, audio

version = "0.1"
cmdPrefix = ";;"
voicePrefix = "$"
client = discord.Client ()

def getPath ():
	return os.path.dirname (os.path.realpath (__file__))

@client.event
async def on_message (message):
	if (message.author == client.user): return
	
	authorID = message.author.id
	guildID = message.channel.guild.id
	msg = message.content
	response = ""

	try:
		voiceChannelID = message.author.voice.channel.id
		voiceChannel = client.get_channel (voiceChannelID)
	except:
		voiceChannel = None

	if (msg.startswith (cmdPrefix.lower ())):
		msg = msg[len (cmdPrefix):].lower ().strip ()

		if (msg.startswith ("help")):
			response += "MLP Voice Generator **v{}** :blue_heart:\n".format (version)
			for entry in textData.helpText:
				response += "- `{}{}`: `{}`\n".format (cmdPrefix, entry, textData.helpText[entry])

			response += "\nYou can talk by sending messages such as `{}your message here`".format (voicePrefix)

		elif (msg.startswith ("voice")):
			msg = " ".join (msg.split ()[1:])
			response = audio.updateAuthorModel (authorID, msg)

	elif (msg.startswith (voicePrefix.lower ())):
		msg = msg[len (voicePrefix):].strip ()
		if (len (msg) == 0): response = textData.errorEmptyMessage
		else: response = await audio.generateAndPlayAudio (msg, authorID, guildID, voiceChannel)

	if (len (response) > 0): await message.channel.send (response)

@client.event
async def on_ready ():
	print (client.user.name + " booted up.")
	await client.change_presence (activity = discord.Game (name = "{}help".format (cmdPrefix)))

try:
	token = open (getPath () + "/token", "r").read ().strip ()
except:
	print ("Please create a 'token' file with your discord bot token in the same directory as the run script.")
	exit (1)

audio.initialize ()
client.run (token)