# MLP Voice Generator

A prototype Discord bot that lets you talk as My Little Pony characters on voice chat. This is all possible thanks to the group effort from the Pony Preservation Project on /mlp/. Without them, we wouldn't be here.

## How to set up:
- Make sure you have Python3 installed (Not the latest 3.8 versions since those don't work with Tensorflow 1.15. Get a python3 version such as Python 3.7)
- Install all the required Python packages: `pip install -r requirements.txt`
- Install ffmpeg and put it in path.
- Create a file named "token" in the folder with the run.py script, and paste your discord bot token in there.
- Download https://drive.google.com/open?id=1DMyL3RxFqAVhH60VCLnVaDt2YJb2RCfz , rename it to `waveglow.pt` and put it in the tacotron folder
- Download pony voices from https://docs.google.com/document/d/1xe1Clvdg6EFFDtIkkFwT-NPLRDPvkV4G675SUKjxVRU/edit#heading=h.fyifxhdx9qqz (the tacotron2 models section), and get only models with the MMI tag. The more iterations a model has, the better it should sound. You can then rename these files as you want (preferably just "Twilight", "Fluttershy", "Discord", etc) and put them under tacotron/voices. The bot will automatically recognize these files as valid options for voice chat.
- Run `python3 run.py` and enjoy!

## Notes
- If you get a warning about tensorflow not being able to open the dynamic library cudart64_something.dll, please install the correpsonding CUDA Toolkit, so that you can have decent performance. If it lacks cudart64_100.dll, then install CUDA 10.0, if it lacks cudart64_101.dll install CUDA 10.1 and so on and so forth.
- Make sure you have an Nvidia GPU with working drivers, otherwise this won't work.
- If you get weird errors such as not being able to install torch/torchvision, you can download the version you need from https://download.pytorch.org/whl/torch_stable.html
- If you get weird errors relating torch not being able to find libraries, try to change your python version, anything below 3.8 will do.