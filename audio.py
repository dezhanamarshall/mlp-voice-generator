import discord, os
import tacotron.driver as driver
import textData

def getPath ():
	return os.path.dirname (os.path.realpath (__file__))

voiceClients = {}

async def makeSureBotIsConnected (guildID, voiceChannel):
	global voiceClients
	if (voiceChannel == None): return textData.errorCantGetVoicechannel

	if (guildID in voiceClients and not voiceClients[guildID].is_connected ()):
		await voiceClients[guildID].disconnect ()
		voiceClients.pop (guildID, None)

	if (guildID in voiceClients and voiceClients[guildID].channel.id != voiceChannel.id):
		await voiceClients[guildID].disconnect ()
		voiceClients.pop (guildID, None)

	if (guildID not in voiceClients):
		voiceClient = await voiceChannel.connect ()
		print ("Connecting to voice channel: {}".format (voiceChannel.id))
		voiceClients[guildID] = voiceClient
	
	return ""

async def disconnect (guildID):
	global voiceClients
	if (guildID not in voiceClients): return errorNotInAnyVoiceChannel
	else: await voiceClients[guildID].disconnect (); voiceClients.pop (guildID, None); return ""

async def generateAndPlayAudio (msg, authorID, guildID, voiceChannel):
	errorMessage = await makeSureBotIsConnected (guildID, voiceChannel)
	if (len (errorMessage) != 0): return errorMessage

	outputFile = getPath () + "/audioOutput/{}.wav".format (guildID)
	driver.generateWAV (msg, authorID, outputFile)

	voiceClient = voiceClients[guildID]
	voiceClient.stop ()
	audioSource = discord.FFmpegPCMAudio (outputFile)
	voiceClient.play (audioSource)

	return msg

def updateAuthorModel (authorID, model):
	return driver.updateAuthorModel (authorID, model)

def initialize ():
	driver.initialize ()